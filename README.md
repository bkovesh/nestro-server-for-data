# Сервис для получения данных

## Запуск

Установка `npm i`  
Запуск сервера `npm run start`  




## Валюта

### Курс валюты на определенную дату с сервиса ЦБ    

http://localhost:5006/currencies/cbr/daily?method=GetCursOnDate&On_date=2023-08-22  

Описание выходных данных https://www.cbr.ru/development/DWS/

### Курс валюты с сервиса cbr-xml-daily.ru

http://localhost:5006/currencies/currency-by-date?currency=USD&date=2023/02/18  




## Котировки нефти

### Котировки из таблицы Excel

Данные берутся из `/files/2.xlsx`  

http://localhost:5006/quotes?sheet=январь&header=A&range=A5:AI25&headers_range=A3:AI3&date=05.01.2023&oil_brand=Brand of Oil 2




## Счета

### Счета из таблицы

Данные берутся из `/files/1.xlsx`  

http://localhost:5006/invoices?sheet=Анализ_БК%2bББ&header=A&range=A4:DM5&headers_range=A1:DM2

`sheet` - имя листа  
`header` - тип заголовков (A - буквенные, 1 - из названий заголовков)  
`range` - диапазон таблицы, с которого берутся данные  
`headers_range` - диапазон таблицы, с которого берутся данные  



## Составные части ПО

`app.js` - файл приложения  
`files` - файлы Excel (Приложения 1, 2, 3), откуда берутся данные  
`src` - основной код ПО  
- `api` - файлы для обращения к сторонним API  
- `bin` - файлы приложения  
- `constants` - глобальные постоянные переменные  
- `helpers` - вспомогательные функции  
- `public` - фронтенд-часть сервера (фактически не используется)  
- `routes` - роуты  
  - `currencies` - получение данных с ЦБ  
  - `invoices` - получение данных сводной таблицы  
  - `quotes` - получение данных таблицы котировок нефти  
- `views` - фронтенд-часть сервера (фактически не используется)  
