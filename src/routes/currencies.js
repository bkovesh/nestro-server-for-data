const express = require('express');
const router = express.Router();
const soap = require('soap');

const RESPONSES = require("../constants/responses");

const { CENTRAL_BANK_OFFICIAL } = require('../constants/api');
const CentralBankService = require("../api/CentralBank");

const CentralBank = new CentralBankService()

router.get('/daily', async (req, res, next) => {
  try {
    const { query } = req
    const result = await CentralBank.getDaily(query)
    return res.json(result)
  } catch (error) {
    console.error(error)
    return res.status(403).json({ status: RESPONSES.SERVER_ERROR });
  }
});

router.get('/currencies-by-date', async (req, res, next) => {
  try {
    const { query } = req
    const result = await CentralBank.getCurrenciesByDate(query)
    return res.json(result)
  } catch (error) {
    console.error(error)
    return res.status(403).json({ status: RESPONSES.SERVER_ERROR });
  }
});

router.get('/currency-by-date', async (req, res, next) => {
  try {
    const { query } = req
    const result = await CentralBank.getCurrencyByDate(query)
    return res.json(result)
  } catch (error) {
    console.error(error)
    return res.status(403).json({ status: RESPONSES.SERVER_ERROR });
  }
});

// OFFICIAL

router.get('/cbr/daily', async (req, res) => {
  try {
    const { query } = req
    const { method } = query ?? {}
    console.log('@daily-cbr', query)
    if (!method) return res.status(404).json({
      status: RESPONSES.SERVER_ERROR,
      message: 'Method not found in query'
    })

    const client = await soap.createClientAsync(CENTRAL_BANK_OFFICIAL);
    console.log('@daily-cbr', client)
    const result = await client?.[method + 'Async']?.(query);

    // console.log('@daily-cbr', result)
    return res.json(result)
  } catch (error) {
    console.error('@daily-cbr', error)
    return res.status(403).json({ status: RESPONSES.SERVER_ERROR });
  }
});

module.exports = router;
