const express = require('express');
const router = express.Router();
const XLSX = require("xlsx");
const fs = require('fs')
const path = require('path')

const RESPONSES = require("../constants/responses");
const QuotesService = require("../helpers/Quotes");

const xlsxToData = ({ query, fileData }) => {
  try {
    const { sheet, range, headers_range, header = 'A', date, oil_brand } = query ?? {}

    const workbook = XLSX.read(fileData, {
      cellDates: true,
      cellNF: false,
      cellText: false
    });
    const worksheet = workbook.Sheets?.[sheet];
    // https://docs.sheetjs.com/docs/api/utilities/array/#array-output
    const headers = XLSX.utils.sheet_to_json(worksheet, {
      header,
      range: headers_range,
      blankrows: true,
      raw: true,
      rawNumbers: true,
      defval: ''
    });
    const data = XLSX.utils.sheet_to_json(worksheet, {
      header,
      range,
      blankrows: true,
      raw: true,
      rawNumbers: true,
      defval: ''
    });

    const Quotes = new QuotesService({ data })
    const result = Quotes.getPriceOfOilBrandByDate({ date, oilBrand: oil_brand, dateHeader: `combined\r\r\ndescription` })

    return { result, headers, data };
  } catch (error) {
    console.error(error)
    throw error;
  }
}

router.get('/', async (req, res, next) => {
  try {
    const { query } = req;
    const fileData = await fs.readFileSync(path.join(__dirname, '../../files/2.xlsx'))
    const result = xlsxToData({ query, fileData })
    return res.json(result);
  } catch (error) {
    console.error(error)
    return res.status(403).json({ status: RESPONSES.SERVER_ERROR });
  }
});

router.post('/', async (req, res, next) => {
  try {
    const { query } = req
    const fileData = req.files.file.data
    const result = xlsxToData({ query, fileData })
    return res.json(result);
  } catch (error) {
    console.error(error)
    return res.status(403).json({ status: RESPONSES.SERVER_ERROR });
  }
});

module.exports = router;
