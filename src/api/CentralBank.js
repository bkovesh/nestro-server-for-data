const axios = require('axios')

const { CENTRAL_BANK } = require('../constants/api')

class CentralBank {
  constructor() {
    this.axios = axios.create({
      baseURL: CENTRAL_BANK,
      timeout: 5000,
      headers: {}
    });
  }

  getDaily = async  () => {
    try {
      const result = await this.axios.get('/daily_json.js')
      return result.data
    } catch (error) {
      console.error('@CentralBank > getDaily', error)
      return null
    }
  }

  // date format YYYY/MM/DD
  getCurrenciesByDate = async  ({ date }) => {
    try {
      const result = await this.axios.get(`/archive/${date}/daily_json.js`)
      return result.data
    } catch (error) {
      console.error('@CentralBank > getCurrenciesByDate', error)
      return null
    }
  }

  // date format YYYY/MM/DD
  getCurrencyByDate = async  ({ currency, date }) => {
    try {
      console.log('@CentralBank > getCurrencyByDate', { currency, date })
      const currencies = await this.getCurrenciesByDate({ date })
      console.log('@CentralBank > getCurrencyByDate', currencies)
      const result = currencies?.Valute?.[currency?.toUpperCase?.()]
      return result
    } catch (error) {
      console.error('@CentralBank > getCurrencyByDate', error)
      return null
    }
  }
}

module.exports = CentralBank