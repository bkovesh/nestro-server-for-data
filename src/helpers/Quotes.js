class Quotes {
  constructor({ data }) {
    this.data = data
  }

  formatDate(dateFromParams = '', plusDays = 0) {
    try {
      const dateFromParamsFormatted = new Date(dateFromParams)
      const date = dateFromParamsFormatted.getDate()
      const month = dateFromParamsFormatted.getMonth()
      const year = dateFromParamsFormatted.getFullYear()
      return `${date+plusDays}.${month+1}.${year}`
    } catch (error) {
      console.error(error)
      return null
    }
  }

  getPriceOfOilBrandByDate({ date = '', oilBrand, dateHeader = 'Data' }) {
    try {
      const dateFromParamsFormatted = this.formatDate(date.split('.').reverse().join('.'))
      let result
      this.data.forEach((item, i) => {
        if (i < 1) return
        const dateFormatted = this.formatDate(item?.[dateHeader], 1)
        if (dateFormatted === dateFromParamsFormatted) result = item?.[oilBrand]
        console.log('@Quotes > getPriceOfOilBrandByDate', item?.[dateHeader], { dateFormatted, dateFromParamsFormatted })
      })
      return result
    } catch (error) {
      console.error(error)
      return null
    }
  }
}

module.exports = Quotes